<?php

/**
 * @file
 * Add alternative menu blocks that add the possibility of trimming
 * some of the top of it's hierarchy so it would start displaying from
 * certain menu items or from a whole level (ex: tertiary).
 *
 * @author William Carrier
 * @author David Lesieur
 */

// TODO: When a menu item is deleted, remove the corresponding entries, if any.// TODO: Add a system to reduce the load in the search of other menu items up the menu tree which requires CPU and DB hits.
// TODO: Look at menu.modules or default menu blocks for the default Root Title.

// Trimming methods
define ('MENU_RESTRICTER_DISABLED', 0);
define ('MENU_RESTRICTER_ENABLED_SHOW_MENU_ALWAYS', 1);
define ('MENU_RESTRICTER_ENABLED_SHOW_MENU_ACTIVE', 2);

/**
 * Implementation of hook_help().
 */
function menu_restricter_help($section) {
  switch ($section) {
    case 'admin/help#menu_restricter':
      $output = '<p>'. t('The menu trim module create copies of menu blocks allowing user with menu modification permissions to trim its parent menus.') .'</p>';
      $output .= '<p>'. t('To make trimmed menus, you must first select the module behavior for each menus in the <a href="@settings">module settings</a>.  Then you need to select appropriately the <em>Trim parent items</em> option in the menus to enable trimming its parents.  At last, you need to display the menu trim blocks.  Now when reaching the enabled menu items, its parents should be trimmed.', array('@settings' => 'admin/settings/menu_restricter')) .'</p>';
      $output .= '<p>'. t('For more information please refer to the <a href="@project">project page</a>.', array('@project' => 'http://drupal.org/project/menu_restricter')) .'</p>';
      return $output;
    case 'admin/settings/menu_restricter':
      $output = '<p>'. t('Choose the menus that need to be trimmed. Each of the menus where trimming is allowed will get a corresponding <em>Menu trim</em> block with appropriate options. You should then <a href="@enable">enable those blocks</a>, and <a href="@edit">edit the menu items</a> to <em>Trim parent items</em>.', array('@enable' => url('admin/build/block')), array('@edit' => url('admin/build/menu'))) .'</p>';
      $output .= '<p>'. t('For example, if you want to show items in its trimmable menu only starting at secondary links, you show edit the primary links with it\'s level at <em>2</em> and having its limit checked.') .'</p>';
      $output .= '<dl>'. t('Options definitions :');
      $output .= '<dt>'. t('Trim Method') .'</dt>';
      $output .= '<dd>'. t('<em>Allow trimming</em> will display a root menu by default.') .'</dd>';
      $output .= '<dt>'. t('Root Title') .'</dt>';
      $output .= '<dd>'. t('Enables a title when displaying a root menu.') .'</dd>';
      $output .= '<dt>'. t('Min. Level') .'</dt>';
      $output .= '<dd>'. t('A minimum level from which starting displaying a menu by default.') .'</dd>';
      $output .= '<dt>'. t('Limit') .'</dt>';
      $output .= '<dd>'. t('Won\'t allow trimmable items to trim under its menu minimum level.') .'</dd>';
      $output .= '<dt>'. t('Dept') .'</dt>';
      $output .= '<dd>'. t('The maximum amount of level that a menu will display.') .'</dd>';
      $output .= '<dt>'. t('Exp.') .'</dt>';
      $output .= '<dd>'. t('Won\'t allow expanded menux to reach after the dept.') .'</dd>';
      $output .= '</dl>';
      return $output;
  }
}

/**
 * Implementation for hook_menu().
 *
 * @see menu_restricter_admin_settings()
 */
function menu_restricter_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/menu_restricter',
      'title' => t('Menu Restricter'),
      'description' => t('Choose what menus need trimming.'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('menu_restricter_admin_settings'),
      'access' => user_access('administer menu trim'),
      'type' => MENU_NORMAL_ITEM,
    );
  }
  return $items;   
}

/**
 * Implementation of hook_perm().
 */
function menu_restricter_perm() {
  return array('administer menu trim');
}

/**
 * Administration page callback.
 *
 * @see theme_menu_restricter_admin_settings()
 * @see menu_restricter_admin_settings_validate()
 * @see menu_restricter_admin_settings_submit()
 */
function menu_restricter_admin_settings() {
  $menu_list = menu_get_root_menus();
  $settings = variable_get('menu_restricter', NULL);

  foreach ($menu_list as $mid => $title) {
    $form[$mid] = array(
      '#tree' => TRUE,
    );
    $form[$mid]['title'] = array(
      '#type' => 'item',
      '#title' => check_plain($title),
    );
    $form[$mid]['method'] = array(
      '#type' => 'select',
      '#options' => array(
        MENU_RESTRICTER_DISABLED => t('Never trim'),
        MENU_RESTRICTER_ENABLED_SHOW_MENU_ALWAYS => t('Allow trimming'),
        MENU_RESTRICTER_ENABLED_SHOW_MENU_ACTIVE => t('Allow trimming, hide when inactive'),
      ),
      '#default_value' => $settings[$mid]['method'] ? $settings[$mid]['method'] : MENU_RESTRICTER_DISABLED,
    );
    $form[$mid]['root_title'] = array(
      '#type' => 'checkbox',
      '#default_value' => $settings[$mid]['root_title'] ? $settings[$mid]['root_title'] : FALSE,
    );
    $form[$mid]['min_level'] = array(
      '#type' => 'textfield',
      '#default_value' => $settings[$mid]['min_level'],
      '#size' => 2,
      '#maxlength' => 2,
    );
    $form[$mid]['min_level_limit'] = array(
      '#type' => 'checkbox',
      '#default_value' => $settings[$mid]['min_level_limit'] ? $settings[$mid]['min_level_limit'] : FALSE,
    );
    $form[$mid]['dept'] = array(
      '#type' => 'textfield',
      '#default_value' => $settings[$mid]['dept'],
      '#size' => 2,
      '#maxlength' => 2,
    );
  }
  return system_settings_form($form);
}

/**
 * Format the settings form.
 *
 * Makes a table separating each menu by row.
 */
function theme_menu_restricter_admin_settings($form) {
  $rows = array();
  foreach ($form as $key => $element) {
    $title1 = $element['title'];
    if (is_array($title1) && $title1['#type'] == 'item') {
      $rows[] = array(drupal_render($form[$key]['title']), drupal_render($form[$key]['method']), drupal_render($form[$key]['root_title']), drupal_render($form[$key]['min_level']), drupal_render($form[$key]['min_level_limit']), drupal_render($form[$key]['dept']));
    }
  }
  // First item must have colspan to include hidden items
  $header = array(array('data' => t('Menu')), array('data' => t('Trim Method')), array('data' => t('Root Title')), array('data' => t('Min. Level')), array('data' => t('Limit')), array('data' => t('Dept')));
  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}

/**
 * Form API callback to validate the settings form.
 *
 * Validate the textfield min_level.
 */
function menu_restricter_admin_settings_validate($form_id, $form_values) {
  foreach ($form_values as $key => $element) {
    // Validate minimum level field
    if (is_array($element)) {
      // Validation must be made in 2 steps or it might skip 0
      if (is_numeric($element['min_level'])) {
        if ($element['min_level'] <= 0) {
        form_set_error($key .'][min_level', t('The minimum level must be an integer greater than zero.'));
        }
      }
      elseif ($element['min_level']) {
        form_set_error($key .'][min_level', t('The minimum level must be an integer greater than zero.'));
      }
      // Validation must be made in 2 steps or it might skip 0
      if (is_numeric($element['dept'])) {
        if ($element['dept'] <= 0) {
        form_set_error($key .'][dept', t('The dept must be an integer greater than zero.'));
        }
      }
      elseif ($element['dept']) {
        form_set_error($key .'][dept', t('The dept must be an integer greater than zero.'));
      }
    }
  }
}

/**
 * Form API callback to submit the settings form.
 *
 * Save language settings in variable
 */
function menu_restricter_admin_settings_submit($form_id, $form_values) {
  foreach ($form_values as $key => $element) {
    // Find appropriate element
    if (is_array($element)) {
      $settings[$key] = array('method' => $element['method'], 'root_title' => $element['root_title'], 'min_level' => $element['min_level'], 'min_level_limit' => $element['min_level_limit'], 'dept' => $element['dept']);
    }
  }
  variable_set('menu_restricter', $settings);
  $settings_set = !db_error();
  if (!$settings_set) {
    drupal_set_message(t('The settings did not save properly.'), 'error');
  }
}

/**
 * Implementation of hook_block().
 */
function menu_restricter_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    /**
     * Display the alternative menus for which respective methods aren't disabled. 
     */
    case 'list':
      $settings = variable_get('menu_restricter', NULL);
      if (is_array($settings)) {
        $root_menus = menu_get_root_menus();
        foreach ($root_menus as $mid => $title) {
          if ($settings[$mid] && $settings[$mid]['method'] != MENU_RESTRICTER_DISABLED) {
            $blocks[$mid]['info'] = check_plain($title) .' ('. t('Menu Restricter') .')';
          }
        }
      }
      return $blocks;
    /**
     * The workflow of the block display is that it start by looking for a minimum
     * level and if there isn't one it simply look for selected items (as version
     * 1.0), if there's one it will compare the active trail level with the minimum
     * level ; if it's smaller, it will only display if it isn't a limit and a
     * selected item is found, if it's higher it will be cropped to the minimum
     * then go through as if it was the same as the minimum which will display if
     * it's a limit or if it isn't will note the minimum level values and search
     * for a selected item and if one is found it's displayed but if none was found
     * the minimum level is displayed.  Except for the default behavior, the
     * displayed menus will be cropped to the dept value.
     *
     * In all cases if nothing is to be displayed and the chosen method is to have
     * the root menu displayed by default, it does that.
     */
    case 'view':
      $settings = variable_get('menu_restricter', NULL);
      if (is_array($settings) && $settings[$delta]['method']) { // disabled method is 0 so it won't pass
        $trail = _menu_get_active_trail_in_submenu($delta);
        if (is_array($trail)) {
          array_shift($trail); // Remove trail root menu item
          $level = $settings[$delta]['min_level'];
          // Verify if there's a minimum level
          if (!is_numeric($level)) {
            if ($first_mid = _get_first_trail($trail)) {
              $data = _block_data_dept($first_mid, $settings[$delta]['dept'], $trail);
            }
          }
          else {
            // Verify if the level is lesser than its minimum
            if (count($trail) < $level) {
              // Verify if menu hasn't a limit, otherwise it reached an end
              if (!$settings[$delta]['min_level_limit']) {
                if ($first_mid = _get_first_trail($trail)) {
                  $data = _block_data_dept($first_mid, $settings[$delta]['dept'], $trail);
                }
              }
            }
            else {
              // Trail up to the minimum level
              $min_trail = array_slice($trail, 0, $level);
              // Retreive minimum level id
              $min_mid = end($min_trail);
              // Verify if menu has a limit
              if ($settings[$delta]['min_level_limit']) {
                $data = _block_data_dept($min_mid, $settings[$delta]['dept'], $trail);
              }
              else {
                if ($first_mid = _get_first_trail($min_trail)) {
                  $data = _block_data_dept($first_mid, $settings[$delta]['dept'], $trail);
                }
                else { // If there wasn't any selected item up the branch
                  $data = _block_data_dept($min_mid, $settings[$delta]['dept'], $trail);
                }
              }
            }
          }
        }
        // Verify if nothing is to be displayed and the chosen method is to have the root menu displayed by default        if (!$data && $settings[$delta]['method'] == MENU_RESTRICTER_ENABLED_SHOW_MENU_ALWAYS) {
          // Show the menu using root item as base
          $item = menu_get_item($delta);
          $data['subject'] = $settings[$delta]['root_title'] ? check_plain($item['title']) : '';
          $data['content'] = theme('menu_tree', $delta);
        }
      }
      return $data;
  }
}

/**
 * Implementation of hook_form_alter().
 *
 * @param $form need to be passed by referrence
 *
 * @see menu_restricter_edit_item_form_submit()
 */
function menu_restricter_form_alter($form_id, &$form) {
  if ($form_id == 'menu_edit_item_form') {
    $settings = variable_get('menu_restricter', NULL);
    $mid = $form['mid']['#value'];
    // Initialisation verification    if (isset($settings, $mid)) {
      $item = menu_get_item($mid);
      // Retreive root menu, based on _menu_get_active_trail()      while ($item['pid']) {        $root = $item['pid'];
        $item = menu_get_item($item['pid']);
      }
      // Verify if current item root has trimming enabled
      if ($settings[$root]['method'] != MENU_RESTRICTER_DISABLED) {
        $form['trim'] = array(
          '#type' => 'checkbox',
          '#title' => t('Trim parent items'),
          '#description' => t('Check this option to trim the parent items when this item or any of its children becomes active in its menu <em>Menu trim</em> corresponding <a href="@block">block</a>, not its default one.', array('@block' => url('admin/build/block'))) . theme_more_help_link('admin/help/menu_restricter'),
          '#default_value' => db_result(db_query("SELECT trim FROM {menu_restricter} WHERE mid=%d", $mid)),
          '#weight' => 1,
        );
        $form['#submit']['menu_restricter_edit_item_form_submit'] = current($form['#submit']);
        // Make sure the button remains at the form's bottom        $form['submit']['#weight'] = 10;      }
    }
  }
}

/**
 * Form submittion customization for edit_item_form.
 * The parameters are the same of the calling function menu_restricter_form_alter.
 */
function menu_restricter_edit_item_form_submit($form_id, $form_values) {
  // Form validation
  if (isset($form_values['trim'])) {
    // Menu trimming option saving and verification
    db_lock_table('menu_restricter');
    $db_result_1 = db_result(db_query("SELECT mid FROM {menu_restricter} WHERE mid=%d", $form_values['mid']));
    // Entry verification
    if ($db_result_1) {
      $db_result_2 = db_query("UPDATE {menu_restricter} SET trim=%d WHERE mid=%d", $form_values['trim'], $form_values['mid']);
    }
    else {
      $db_result_2 = db_query("INSERT INTO {menu_restricter} (mid, trim) VALUES (%d, %d)", $form_values['mid'], $form_values['trim']);
    }
    if (db_error() || !$db_result_2) { 
      drupal_set_message(t('A database issue prevented Menu trim to modify its status for the menu %menu.', array('%menu' => $form_values['mid'])), 'error');
    }
    db_unlock_tables();
  }
}

/**
 * Create block data limiting the trail to the dept.
 *
 * @param $mid Default menu ID.
 * @param $dept Maximum dept of the menu to create.
 * @param $trail Menu ID trail.
 *
 * @return Array containing the block data.
 */
function _block_data_dept($mid, $dept, $trail) {
  if (is_numeric($dept) && is_array($trail) && $trail) {
    $dept_search = 0;
    foreach (array_reverse($trail) as $mid_search) {
      if ($dept_search == $dept || $mid_search == $mid) {
        break;
      }
      $dept_search++;
    }
    $item = menu_get_item($mid_search);
    $data['subject'] = check_plain($item['title']);
    $data['content'] = theme('menu_tree', $mid_search);
  }
  else {
    $item = menu_get_item($mid);
    $data['subject'] = check_plain($item['title']);
    $data['content'] = theme('menu_tree', $mid);
  }
  return $data;
}

/**
 * Get the first menu marked as trimmed.
 *
 * @param $trail Menu ID trail.
 *
 * @return Menu ID or NULL if nothing was found.
 *
 * @see _get_trail_data()
 */
function _get_first_trail($trail) {
  if ($trail_trims = _get_trail_data($trail)) {
    foreach (array_reverse($trail) as $trail_mid) {
      if ($trail_trims[$trail_mid]) { // Verify if marked as trimmed.
        return $trail_mid;
      }
    }
  }
  return;
}

/**
 * Get the data from the trail menus.
 *
 * @param $trail Menu ID trail.
 *
 * @return Array with trim results keyed to their Menu ID or NULL if nothing was found.
 */
function _get_trail_data($trail) {
  if ($trail) { // Empty IN argument would make an infinite query loop 
    $trail_trims_query = db_query("SELECT mid, trim FROM {menu_restricter} WHERE mid IN (%s)", implode(",", $trail));
    while (($trail_trim = db_fetch_array($trail_trims_query)) !== FALSE) {
      $trail_trims[$trail_trim['mid']] = $trail_trim['trim'];
    }
  }
  return $trail_trims; // Could be NULL.
}

